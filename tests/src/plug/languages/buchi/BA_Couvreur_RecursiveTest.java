package plug.languages.buchi;

import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.explorer.buchi.scc.BA_Couvreur_Recursive;

public class BA_Couvreur_RecursiveTest extends AbstractFiacreLTLTests {
    @Override
    IExecutionController getControllerInstance(ITransitionRelation runtime) {
        return new BA_Couvreur_Recursive(runtime);
    }
}

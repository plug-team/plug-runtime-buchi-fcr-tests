package plug.languages.buchi;

import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.explorer.buchi.nested_dfs.BA_GaiserSchwoon_Iterative;
import plug.statespace.SimpleStateSpaceManager;

public class GaiserSchwoonIterativeTest extends AbstractFiacreLTLTests {

    @Override
    IExecutionController getControllerInstance(ITransitionRelation runtime) {
        return new BA_GaiserSchwoon_Iterative(runtime, new SimpleStateSpaceManager());
    }
}

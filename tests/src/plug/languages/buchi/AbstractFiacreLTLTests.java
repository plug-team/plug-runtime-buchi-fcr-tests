package plug.languages.buchi;

import java.io.PrintWriter;
import java.nio.file.Paths;
import org.junit.Assert;
import org.junit.Test;
import plug.core.ILanguagePlugin;
import plug.core.ITransitionRelation;
import plug.core.RuntimeDescription;
import plug.core.execution.IExecutionController;
import plug.events.PropertyEvent;
import plug.language.buchi.runtime.BuchiRuntime;
import plug.language.buchikripke.runtime.KripkeBuchiProductSemantics;
import plug.language.fiacre.FiacrePlugin;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.LTL.parser.Parser;
import properties.LTL.transformations.LTL2Buchi;
import properties.PropositionalLogic.PropositionalLogicModel.DeclarationBlock;
import properties.PropositionalLogic.PropositionalLogicModel.Expression;
import properties.PropositionalLogic.PropositionalLogicModel.ExpressionDeclaration;

public abstract class AbstractFiacreLTLTests {
    ILanguagePlugin fiacreModule = new FiacrePlugin();

    abstract IExecutionController getControllerInstance(ITransitionRelation runtime);

    ITransitionRelation getFiacreRuntime(String fileName) throws Exception {
        return fiacreModule.getLoader().getRuntime(fileName);
    }

    BuchiDeclaration getBuchiDeclaration(String ltlFormula) {
        DeclarationBlock propertiesBlock = Parser.parseBlock(ltlFormula);
        Expression property = ((ExpressionDeclaration)propertiesBlock.getDeclarations().iterator().next()).getExpression();
        LTL2Buchi convertor = new LTL2Buchi(new PrintWriter(System.out));

        BuchiDeclaration decl = convertor.convert(property);
        return decl;
    }

    void verify(String fileName, String ltl, boolean verified) throws Exception {
        BuchiDeclaration buchiAutomaton = getBuchiDeclaration(ltl);
        BuchiRuntime buchiRuntime = new BuchiRuntime(buchiAutomaton);
        RuntimeDescription kripke = new RuntimeDescription(Paths.get(fileName));

        KripkeBuchiProductSemantics kbProductSemantics = new KripkeBuchiProductSemantics(kripke, buchiRuntime);
        IExecutionController controller = getControllerInstance(kbProductSemantics);

        boolean[] result = new boolean[] { true };
        controller.getAnnouncer().when(PropertyEvent.class, (announcer, event) -> {
           result[0] &= event.isVerified();
        });
        controller.execute();

        if (result[0] != verified) {
            Assert.fail("Property " + (result[0] ? "is verified but shouldn't" : "isn't verified but should"));
        }
    }

    @Test
    public void testMutualExclusionInitNOK() throws Exception {
        String ltl = "exclusion = []!(|{Alice}1@I| && |{Bob}1@I|)";
        verify("tests/resources/AliceBob0.fcr", ltl, false);
    }

    @Test
    public void testMutualExclusionNOK() throws Exception {
        String ltl = "exclusion = []!(|{Alice}1@CS| && |{Bob}1@CS|)";
        verify("tests/resources/AliceBob0.fcr", ltl, false);
    }

    @Test
    public void testMutualExclusionOK() throws Exception {
        String ltl = "exclusion = []!(|{Alice}1@CS| && |{Bob}1@CS|)";
        verify("tests/resources/AliceBob1.fcr", ltl, true);
    }

    @Test
    public void testEventuallyCS2() throws Exception {
        String ltl = "evcs = []<>(|{Alice}1@CS| || |{Bob}1@CS|)";
        verify("tests/resources/AliceBob2.fcr", ltl, false);
    }

    @Test
    public void testEventuallyCS3() throws Exception {
        String ltl = "evcs = []<>(|{Alice}1@CS| || |{Bob}1@CS|)";
        verify("tests/resources/AliceBob3.fcr", ltl, true);
    }

    @Test
    public void testEventuallyCSPeterson() throws Exception {
        String ltl = "evcs = []<>(|{Alice}1@CS| || |{Bob}1@CS|)";
        verify("tests/resources/AliceBobMeetPeterson.fcr", ltl, true);
    }

    @Test
    public void testFairness1() throws Exception {
        String ltl = "fairness = [] ((|{sys}1:flags[0] = true| -> <> |{Alice}1@CS|) && (|{sys}1:flags[1] = true| -> <> |{Bob}1@CS|))";
        verify("tests/resources/AliceBob1.fcr", ltl, false);
    }

    @Test
    public void testFairness2() throws Exception {
        String ltl = "fairness = [] ((|{sys}1:flags[0] = true| -> <> |{Alice}1@CS|) && (|{sys}1:flags[1] = true| -> <> |{Bob}1@CS|))";
        verify("tests/resources/AliceBob2.fcr", ltl, false);
    }

    @Test
    public void testFairness3() throws Exception {
        String ltl = "fairness = [] ((|{sys}1:flags[0] = true| -> <> |{Alice}1@CS|) && (|{sys}1:flags[1] = true| -> <> |{Bob}1@CS|))";
        verify("tests/resources/AliceBob3.fcr", ltl, false);
    }

    @Test
    public void testFairnessPetterson() throws Exception {
        String ltl = "fairness = [] ((|{sys}1:flags[0] = true| -> <> |{Alice}1@CS|) && (|{sys}1:flags[1] = true| -> <> |{Bob}1@CS|))";
        verify("tests/resources/AliceBobMeetPeterson.fcr", ltl, true);
    }

    @Test
    public void testBobInfoftenPetterson() throws Exception {
        String ltl = "infoften = [] <> |{Bob}1@CS|";
        verify("tests/resources/AliceBobMeetPeterson.fcr", ltl, false);
    }
}

package plug.languages.buchi;

import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.explorer.buchi.nested_dfs.BA_GaiserSchwoon_Recursive;


public class GaiserSchwoonRecursiveTest extends AbstractFiacreLTLTests {

    @Override
    IExecutionController getControllerInstance(ITransitionRelation runtime) {
        return new BA_GaiserSchwoon_Recursive(runtime);
    }
}

package plug.languages.buchi;

import plug.core.ITransitionRelation;
import plug.core.execution.IExecutionController;
import plug.explorer.buchi.nested_dfs.BA_CVWI_Recursive;

public class BA_CVWI_RecursiveTest extends AbstractFiacreLTLTests {
    @Override
    IExecutionController getControllerInstance(ITransitionRelation runtime) {
        return new BA_CVWI_Recursive(runtime);
    }
}
